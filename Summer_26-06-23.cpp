﻿#include <iostream>
#include <ctime>
#include <ctime>

using namespace std;

double getShotPointX();
double getShotPointY();
bool isInArea(double, double);
bool isHit(double, double);
double percentOfHits(int, int);
int inputData();



int main()
{
	srand(time(0));
	int n = inputData();
	int i = 0;
	double x;
	double y;
	bool t;
	int counter = 0;
	int shots = 0;
	do {
		x = getShotPointX();
		y = getShotPointY();
		t = isInArea(x, y);
		if(t == true)
		{
			counter++;
		}
		shots++;
	} while (shots < n);
	if ((x == 1.3) && (y == 0.2)) 
	{
		cout << "You get in stop point (1.3, 0.2)";
		system("pause");
		system("cls");
	} else
	cout << "percent Of Hits: " << percentOfHits(counter, shots) << endl;
}

int inputData()
{
	int n;
	while (true)
	{
		cout << "Enter quantity of tries n >= 0 and n <= 1000: ";
		cin >> n;
		if (n >= 0 && n <= 1000)
		{
			return n;
		}
		cout << "Invalid n. Try again." << endl;
		system("pause");
		system("cls");
	}
}

double getShotPointX()
{
	int a = -2;
	int b = 2;
	double x = a + rand() % (b - a + 1);
	return x;
}

double getShotPointY()
{
	int a = -2;
	int b = 2;
	double y = a + rand() % (b - a + 1);

	return y;
}

bool isInArea(double x, double y)
{
	if (((x >= -1) && (y <= -x - 1) && (y >= -1)) || ((x >= -1) && (y <= 1) && (y >= x + 1)) || ((x * x + x * x <= 1) && (x >= 0) && (x <= 1)))
		return true;
	else return false;
}

double percentOfHits(int counter, int shots)
{
	return (((double) counter) / shots) * 100;
}
